#LiveFootballTickets frontend

- Scaffolded with https://github.com/yeoman/generator-gulp-webapp

###Run it locally:

- in the root folder run `npm install`, and then `gulp serve`
- to build the dist version `gulp build`

## asp notes
look for @k6temp


# LiveFootballTickets
> Latest snapshot of production code

Repository: https://gitlab.com/kuus/livefootballtickets

### Pages Preview
- Club: https://kuus.github.io/club.html
- Content: https://kuus.github.io/content.html
- Home: https://kuus.github.io/
- League: https://kuus.github.io/league.html
- Match: https://kuus.github.io/match.html
- News: https://kuus.github.io/news.html
- Purchase: https://kuus.github.io/purchase.html

### Static Files Sources
- Styles: https://gitlab.com/kuus/livefootballtickets/tree/master/dist-static/styles
- Scripts: https://gitlab.com/kuus/livefootballtickets/tree/master/dist-static/scripts

### Data Source
- https://gitlab.com/kuus/livefootballtickets/tree/master/app/data

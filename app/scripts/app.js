/* global jQuery, Bloodhound */
/* jshint devel:true, unused: false */

/*! @license credits */

(function (window, document, $) {
  'use strict';

  /**
   * Debouncer utility
   * @param  {function} func
   * @param  {int}      timeoutArg
   * @return {function}
   */
  function debouncer (func , timeoutArg) {
    var timeoutID;
    var timeout = timeoutArg || 200;
    return function () {
      var scope = this;
      var args = arguments;
      clearTimeout(timeoutID);
      timeoutID = setTimeout(function () {
        func.apply(scope, Array.prototype.slice.call(args));
      }, timeout);
    };
  }

  /**
   * Initialize readmore areas,
   * using readmore.js jQuery plugin
   *
   * @link(http://jedfoster.com/Readmore.js/)
   */
  function initReadmore() {
    var $readmoreAreas = $('.readmore');

    $readmoreAreas.readmore({
      speed: 180,
      moreLink: '<div class="readmore-toggle"><a href="#">Read more</a> <i class="icon icon-angle-down"></i></div>',
      lessLink: '<div class="readmore-toggle"><a href="#">Close</a> <i class="icon icon-angle-up"></i></div>',
      afterToggle: function (trigger, element, expanded) {
        console.log('ciao');
        $(element).toggleClass('expanded', expanded);
        // return expanded;
      }
    });
  }

  /**
   * Initialize notifications
   * using bootstrap-notify plugin
   *
   * @link(http://bootstrap-notify.remabledesigns.com/)
   */
  function initNotifications() {
    $.notifyDefaults({
      newest_on_top: true,
      autoHide: false,
      placement: {
        from: 'bottom',
        align: 'right'
      },
      animate: {
        delay: 2000,
        enter: 'animated zoomInDown',
        exit: 'animated zoomOutUp'
      },
      spacing: 60
    });
  }

  /**
   * Initialize search box.
   * It uses Typeahead and Bloodhound
   *
   * @link(https://twitter.github.io/typeahead.js/examples/)
   */
  function initSearch (mockedAPI) {
    var $searchBoxMain = $('#search-main');
    var SEARCH_API = location.protocol + '//' + location.hostname + '/pages/ajax/callbacks.aspx/suggestion#%QUERY';
    var SEARCH_DEEPLINK = 'search?q='; // @todo
    var remoteUrl = window.LTF_URL_SEARCH || SEARCH_API;

    // remoteUrl = 'http://staging.livefootballtickets.com/pages/ajax/callbacks.aspx/suggestion#%QUERY';
    if (mockedAPI || window.LTF_MOCKED_API) {
      remoteUrl = 'data/search.json';
      mockedAPI = true;
    }

    /**
     * Create search reusable function
     * @param  {?string} datumTokenizr
     * @param  {?string} resultsKey
     * @param  {?boolean} more
     * @return {Bloodhound}
     */
    var _createSearch = function (datumTokenizr, resultsKey, more) {
      return new Bloodhound({
        initalize: false,
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace(datumTokenizr),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
          url: remoteUrl,
          wildcard: mockedAPI ? '' : '%QUERY',
          prepare: function (query, settings) {
            if (mockedAPI) {
              return settings;
            }
            settings.type = 'POST';
            settings.contentType = 'application/json; charset=UTF-8';
            settings.data = JSON.stringify({ q: query });
            return settings;
          },
          filter: function (response) {
            if (!mockedAPI) {
              response = JSON.parse(response.d);
            }
            if (more) {
              return response.more ? [true] : [];
            } else {
              return response.result[resultsKey];
            }
          }
        }
      });
    };
    // var top = _createSearch('name', 'top');
    var events = _createSearch('name', 'events');
    var categories = _createSearch('name', 'categories');
    var teams = _createSearch('name', 'teams');
    var stadiums = _createSearch('name', 'stadiums');
    var cities = _createSearch('name', 'cities');
    var more = _createSearch(null, null, true);

    $searchBoxMain.typeahead({
      highlight: true,
    // }, {
    //   name: 'top',
    //   display: 'name',
    //   source: top.ttAdapter(),
    //   templates: {
    //     suggestion: function (data) {
    //       return '' +
    //         '<a href="' + data.url + '" data-id="' + data.id + '">' + data.name +
    //           '<span class="tt__subhint"> - Top result</span>' +
    //         '</a>';
    //     }
    //   }
    }, {
      name: 'events',
      display: 'name',
      limit: 3, // 5 is the default
      source: events.ttAdapter(),
      templates: {
        header: '<h3 class="tt-header">Events</h3>',
        suggestion: function (data) {
          return '' +
            '<a href="' + data.url + '" data-id="' + data.id + '">' + data.name +
              '<span class="tt__subhint tt__date">' + data.date + '</span>' +
            '</a>';
        }
      }
    }, {
      name: 'categories',
      display: 'name',
      limit: 3, // 5 is the default
      source: categories.ttAdapter(),
      templates: {
        header: '<h3 class="tt-header">Categories</h3>',
        suggestion: function (data) {
          return '<a href="' + data.url + '" data-id="' + data.id + '">' + data.name + '</a>';
        }
      }
    }, {
      name: 'teams',
      display: 'name',
      limit: 3, // 5 is the default
      source: teams.ttAdapter(),
      templates: {
        header: '<h3 class="tt-header">Teams</h3>',
        suggestion: function (data) {
          return '<a href="' + data.url + '" data-id="' + data.id + '">' + data.name + '</a>';
        }
      }
    }, {
      name: 'stadiums',
      display: 'name',
      limit: 3, // 5 is the default
      source: stadiums.ttAdapter(),
      templates: {
        header: '<h3 class="tt-header">Stadiums</h3>',
        suggestion: function (data) {
          return '<a href="' + data.url + '" data-id="' + data.id + '">' + data.name + '</a>';
        }
      }
    }, {
      name: 'cities',
      display: 'name',
      limit: 3, // 5 is the default
      source: cities.ttAdapter(),
      templates: {
        header: '<h3 class="tt-header">Cities</h3>',
        suggestion: function (data) {
          return '<a href="' + data.url + '" data-id="' + data.id + '">' + data.name + '</a>';
        }
      }
    }, {
      name: 'more',
      display: 'name',
      limit: 3, // 5 is the default
      source: more.ttAdapter(),
      templates: {
        footer: function (data) {
          var searchUrl = SEARCH_DEEPLINK + data.query; // @todo
          return '<a href="' + searchUrl + '" class="tt__footer">View all results for <strong>\'' + data.query + '\'</strong></a>';
        },
        suggestion: function () {
          return '<i></i>';
        }
      }
    })
    .on('typeahead:asyncrequest', function () {
      $searchBoxMain.addClass('tt-loading');
    })
    .on('typeahead:asyncreceive', function () {
      $searchBoxMain.removeClass('tt-loading');
    })
    .on('typeahead:selected', function (evt, item) {
      if (item.url) {
        window.location.href = item.url;
      }
    })
  }

  /**
   * Initialize sliders on the home page,
   * using slick.js
   *
   * @link(https://github.com/kenwheeler/slick)
   */
  function initSliders() {
    var $slider = $('#header-slider-main');
    var $sliderPlaceholder = $('#header-slider-placeholder');
    var $sliderNav = $('#header-slider-nav-wrap');
    var $thumbnails = $sliderNav.find('li');

    $slider.slick({
      arrows: false
    }).fadeIn();
    $sliderNav.fadeIn();
    $sliderPlaceholder.hide();//fadeOut(400, function () {
      // $sliderPlaceholder.remove();
    // });
    $(window).resize();

    $thumbnails.on('mouseenter', function () {
      var $this = $(this);
      $slider.slick('slickGoTo', $this.index(), false);
      $thumbnails.removeClass('active');
      $this.addClass('active');
    });
  }

  /**
   * Mobile Welcome modal
   * Show it only once through cookies,
   * using jQuery.cookie plugin
   *
   * @link(https://github.com/carhartl/jquery-cookie)
   */
  function initModals () {
    var COOKIE_NAME_WELCOME = 'welcomeMobile';
    var $modalWelcome = $('#modal-welcome');
    var $modalWelcomeBtnContinue = $('#modal-welcome-btn-continue');
    var cookieOnLoad = $.cookie(COOKIE_NAME_WELCOME);

    if (!cookieOnLoad && window.innerWidth < 992) {
      $modalWelcome.modal().on('shown.bs.modal', function () {
        $.cookie(COOKIE_NAME_WELCOME, '1', { expires: 365, path: '/' });
        // maybe analytics tracking
      })
      .on('hide.bs.modal', function () {
        // maybe analytics tracking
      });
      $modalWelcomeBtnContinue.click(function () {
        // maybe analytics tracking
      });
    }

    // remote modals
    $('.modal-remote').click(function (e) {
      var link = this;
      var $modal = $(link.getAttribute('data-modal'));
      var $modalBody = $modal.find('.modal-body');
      $modal.on('show.bs.modal', function () {
        $modalBody.load(link.href);
      })
      .modal();
      e.preventDefault();
    });
  }

  /**
   * Behavior of event list row click
   *
   */
  function initLinks() {
    $('.event-list-item').on('click', function (event) {
      if ( ! $(event.target).is('a') ) {
        var $this = $(this);
        var mainLink = $this.find('a.btn')[0];
        if (mainLink) {
          document.location.href = mainLink.href;
        }
      }
    });
  }

  /**
   * Bootstrap tooltips need to be manually instantiated
   *
   */
  function initTooltips() {
    $('.tip').tooltip({
      container: 'body'
    });
  }

  /**
   * Manage the responsiveness of certain elements
   * on the website: the sticky header, some grid blocks
   * which has a weird behavior (they are positioned quite
   * differently through the various breakpoints),
   * impossible to obtain through CSS only.
   * We use closures here.
   */
  function initResponsiveness() {

    var $window = $(window);
    var $body = $('body');
    var winWidth = window.innerWidth;
    var winScroll = $window.scrollTop();
    var $whyBook = $('#why-book-with-us');
    var $about = $('#about');
    var $headerTop = $('.header-top');
    var scrollBreakpoint = $headerTop.outerHeight();
    var $stickyHeader = $('.header-middle');
    var isStickyCloned = false;
    var isSticked = false;
    var $colTickets = $('#col-tickets');
    var $colStadium = $('#col-stadium');
    var stadiumImg = document.getElementById('stadium-img');
    var stadiumImageHasLoaded = false;
    var $ticketsList = $('#tickets-list');
    var $dropdownMenus = $('#navbar').find('.dropdown-menu');

    /**
     * Create sticky header clone.
     */
    var createStickeHeaderClone = function () {
      $stickyHeader
        .addClass('sticky-header')
        .clone().addClass('sticky-header-clone').insertAfter($stickyHeader);
      isStickyCloned = true;
    };
    /**
     * Manage header stickyness (only on lg screens).
     */
    var manageStickyHeader = function () {
      if (winScroll >= scrollBreakpoint && winWidth >= 992 && !isSticked) {
        if (!isStickyCloned) {
          createStickeHeaderClone();
        }
        $body.addClass('sticky-header-on');
        isSticked = true;
      }
      if (((winScroll < scrollBreakpoint) && isSticked) || winWidth < 992 && isSticked) {
        $body.removeClass('sticky-header-on');
        isSticked = false;
      }
    };
    if (winWidth >= 992) {
      createStickeHeaderClone();
    }
    /**
     * Fix layout grid on tablets.
     * This function is the good place where to put other
     * responsive tweaks.
     */
    var syncBlocksHeight = function () {
      // correspond to: @media (min-width: $screen-sm-min) and (max-width: $screen-sm-max)
      if (winWidth >= 768 && winWidth < 992) {
        $about.css('min-height', $whyBook.outerHeight());
      } else {
        $about.css('min-height', 0);
      }
    };
    /**
     * Give a maximum height to the ticket list
     * Wait for the stadium image to load before to do the calculation
     * or just sync if there is no image.
     */
    var trySyncTicketsPageColsHeight = function () {
      if (!$colTickets.length || !$colStadium.length || !$ticketsList.length) {
        return;
      }
      if (stadiumImg && stadiumImg.src && !stadiumImageHasLoaded) {
        var image = new Image();
        image.src = stadiumImg.src;
        image.onload = maybeSyncTicketsPageColsHeight;
        stadiumImageHasLoaded = true;
      } else {
        maybeSyncTicketsPageColsHeight();
        stadiumImageHasLoaded = true;
      }
    };
    /**
     * Actually sync the maximum height of the ticket list
     * with the column beside only on big screens.
     */
    var maybeSyncTicketsPageColsHeight = function () {
      // correspond to: @media (min-width: $screen-md-min)
      if (winWidth >= 992) {
        var heightLimit = $colStadium.outerHeight();
        var targetHeight = heightLimit - 90;
        $ticketsList.css({
          'max-height': targetHeight
        });
      } else {
        $ticketsList.css({
          'max-height': 'none'
        });
      }
    };
    /**
     * Make the menu dropdowns scrollable
     * if they don't fit in the screen
     */
    var maximumHeightToDropdowns = function () {
      // correspond to: @media (min-width: $grid-float-breakpoint)
      if (winWidth >= 768) {
        $dropdownMenus.css({
          // -20 just to give a little breath in the bottom
          'max-height': window.innerHeight - $stickyHeader.outerHeight() - 20,
          'overflow': 'auto'
        });
      } else {
        $dropdownMenus.css({
          'max-height': 'none',
          'overflow': 'auto'
        });
      }
    };

    // on scroll
    $window.scroll(function() {
      winScroll = $window.scrollTop();
      manageStickyHeader();
    });

    // on resize
    $window.resize(function () {
      winWidth = window.innerWidth;
      syncBlocksHeight();
      manageStickyHeader();
      maximumHeightToDropdowns();
    });
    // on resize (debounced)
    $window.resize(debouncer(function () {
      trySyncTicketsPageColsHeight();
    }));

    // on ready as well
    syncBlocksHeight();
    trySyncTicketsPageColsHeight();
    manageStickyHeader();
    maximumHeightToDropdowns();
  }

  /**
   * Copied from staging.livefootballtickets.com
   *
   */
  function onEmailSubscription () {
    $('.newsletter .help-block').hide();
    var hasError = false;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    var emailaddressVal = $('#ctl00_ucEmailSubscription_UserEmail').val();
    if (emailaddressVal === '' || emailaddressVal === 'enter your email') {
      $('.newsletter .input-group').after('<div class="help-block">This field can not be empty</div>');
      hasError = true;
    }
    else if (!emailReg.test(emailaddressVal)) {
      $('.newsletter #input-group').after('<div class="help-block">Enter a valid email address.</span>');
      hasError = true;
    }

    if (hasError) {
      $('.newsletter .input-group').addClass('has-error');
      return false;
    }

    var params = '{email:\'' + emailaddressVal + '\'}';

    $.ajax({
      type: 'POST',
      url: location.protocol + '//' + location.hostname + '/pages/ajax/callbacks.aspx/emailsubscription',
      data: params,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        // xhr.setRequestHeader('Content-length', params.length);
      },
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      success: function (msg, status) {
        $('.newsletter .input-group').after('<div class="help-block">' + msg.d + '</span>');
      },
      error: function (xhr, msg, e) { /*alert(msg);*/ }
    });
  }

  /**
   * On ready
   *
   */
  $(document).ready(function () {
    initResponsiveness();
    initReadmore();
    initNotifications();
    initSearch();
    initSliders();
    initModals();
    initLinks();
    initTooltips();

    $('#emailSubscribtion').click(onEmailSubscription);
  });
})(window, document, jQuery);
